from django.urls import path, include
from rest_framework.routers import DefaultRouter, SimpleRouter

from series.views import SerieViewSet, ChapterViewSet

# Use DefaultRouter to see API root view (urls)
router = SimpleRouter()
router.register(r'series', SerieViewSet, basename="series")
router.register(r'chapters', ChapterViewSet, basename="chapters")

# urlpatterns = router.urls
urlpatterns = [
    path('', include(router.urls)),
]



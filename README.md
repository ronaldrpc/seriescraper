
# Series scraper

> This project can scrap the last chapter of a manga, manhwa, webtoons, etc.
> series from [lectortmo.com](https://lectortmo.com/) and 
> [webtoons.com](https://www.webtoons.com/) websites.
> 
> *At the moment, it only supports both websites.* 
> 


## Dependencies

| Name                  | Version |
|-----------------------|---------|
| Python                | 3.8     |
| Django                | 4.0.4   |
| Request               | 2.27.1  |
| Celery                | 5.2.6   |
| BeautifulSoup         | 4.11.1  |
| Django Rest Framework | 3.13.1  |


## Installation

- Clone the repository.
  - https://gitlab.com/ronaldrpc/seriescraper
- Create the environment (using venv or pipenv)
- Install dependencies (using requirements.txt or Pipfile)
- Create .env file based on .env.local_example
- Run migrations (python manage.py migrate)
- Create superuser
- Run server


## Rabbitmq and celery configuration

*Note: You have to have rabbitmq installed on your machine.*

The next steps are optional, but it's recommended to following them.

If you want to skip those steps (1 to 4), please add in your .env file
the next configuration`CELERY_BROKER_URL=amqp://guest:guest@127.0.0.1:5672/`


1) Create a rabbitmq user 
```bash
sudo rabbitmqctl add_user <USER> <PASS>
```

2) Create a virtual host
```bash
sudo rabbitmqctl add_vhost <V_HOST>
```

3) Set permissions to user and vhost
```bash
sudo rabbitmqctl set_permissions -p <V_HOST> <USER> ".*" ".*" ".*" 
```

4) Then, replace user, pass and vhost values in the env variable `CELERY_BROKER_URL` (located in .env_local_example file).

For example: `CELERY_BROKER_URL=amqp://myuser:mypass@127.0.0.1:5672/myvhost`.


5) 
Another env variable to configure is `CELERY_RESULT_BACKEND`.
In this case, just replace `<DBNAME>` by the name of the database where celery results will be saved.


## Running celery

It's recommended to execute celery worker first
```bash
celery -A seriescraper worker -l INFO
```

Then run celery beat
```bash
celery -A seriescraper beat
```

.

## Demo

Go to `http://127.0.0.1:8000/` and you will see a form like
the image below. In this form, you can register a series to be 
scraped and get its last chapter released at the date.

![Register form](doc/assets/images/register-formv2.png)

For example, let's try with Black clover manga from 
lectortmo website:

```angular2html
Series name = Black clover

Episode list url = https://lectortmo.com/library/manga/10457/black-clover
```

Once you fill the form and submit the data, the page will 
reload by itself and next you will be able to see the 
series registered. It will look like this.

![Series list](doc/assets/images/series-list.png)

After each reload you can see the most recent chapters
table (under the series table) from each registered series.

![Recent chapters](doc/assets/images/recent-chapters.png)


### Keep in mind

- Each series can be edited or deleted.
- Each chapter also can be edited or deleted.
  - You can only modify the `seen` status. 
- Celery beat schedule is set to run a periodic task each 3 minutes.

> *The end*
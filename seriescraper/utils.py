from django.db import models
from django.contrib import admin


class BaseModel(models.Model):
    """Abstract base class for models"""
    created_at = models.DateTimeField('created_at', auto_now_add=True)
    updated_at = models.DateTimeField('updated_at', auto_now=True)

    class Meta:
        abstract = True
        ordering = ['-updated_at']


class BaseModelAdmin(admin.ModelAdmin):
    """Base model for admin purposes."""
    actions = [
        'change_is_seen_true',
        'change_is_seen_false',
    ]

    # def change_is_seen_true(self, request, queryset):
    #     queryset.update(is_seen=True)
    #     self.message_user(request, f'{queryset.count()} objects updated')
    # change_is_seen_true.short_description = "Make selected is seen to true"
    #
    # def change_is_seen_false(self, request, queryset):
    #     queryset.update(is_seen=False)
    #     self.message_user(request, f'{queryset.count()} objects updated')
    # change_is_seen_false.short_description = "Make selected is seen to false"



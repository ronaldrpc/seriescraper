
// ---------------------------------------------------------------------------------------------------
// API

const seriesURL = "http://127.0.0.1:8000/api/series/";
const chapterURL = "http://127.0.0.1:8000/api/chapters/";

async function fetchData(endpointUrl) {
    const response = await fetch(endpointUrl);
    return await response.json();
}

async function postSeriesForm(jsonData) {
    const fetchOptions = {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			Accept: "application/json",
		},
		body: jsonData,
	};
    const response = await fetch(seriesURL, fetchOptions);
    return await response.json();
}

async function fetchActionForm(jsonData, method, url) {
    // PUT and DELETE methods
    const fetchOptions = {
		method: method,
		headers: {
			"Content-Type": "application/json",
			Accept: "application/json",
		},
		body: jsonData,
	};
    const response = await fetch(url, fetchOptions);
    return await response.json();
}

// End API
// ------------------------------------------------------


// ------------------------------------------------------
// Tables

function customizeColumName(name) {
    const newName = name === "id" ? "#" : name;
    const capitalized_name = newName.charAt(0).toUpperCase() + newName.slice(1)
    return capitalized_name.replaceAll("_", " ")
}

const addValuesInHeader = (columns, headId) => {
    const tHead = document.getElementById(headId);
    const row = tHead.insertRow();
    columns.forEach(column => {
        let th = document.createElement('th');
        th.innerHTML = customizeColumName(column);
        row.appendChild(th);
    });
}

function createSeriesRow(index, detailUrl, serie) {
    return `
        <tr>
            <th>${index + 1}</th>
            <td>${serie.name}</td>
            <td><a href="${serie.episode_list_url}" target="_blank">${serie.episode_list_url}</a></td>
            <td>
                <button id="${serie.id}" type="button" class="btn btn-outline-success" data-bs-toggle="modal" 
                data-bs-target="#series-modal" onclick="seriesActionManager(${serie.id}, 'update', '${detailUrl}')">
                    Edit
                </button>
            </td>
            <td>
                <button id="${serie.id}" type="button" class="btn btn-danger" data-bs-toggle="modal" 
                data-bs-target="#series-modal" onclick="seriesActionManager(${serie.id}, 'delete', '${detailUrl}')">
                    Delete
                </button>
            </td>
        </tr>
    `;
}

function createChapterRow(index, detailUrl, chapter) {
    let seen = chapter.is_seen ? "Seen" : "Not seen";
    return `
        <tr>
            <th>${index + 1}</th>
            <td>${chapter.serie_name}</td>
            <td><a href="${chapter.link}" target="_blank">${chapter.title}</a></td>
            <td>${chapter.release_date}</td>
            <td>${seen}</td>
            <td>
                <button id="chapter-${chapter.id}" type="button" class="btn btn-outline-success" data-bs-toggle="modal" 
                data-bs-target="#chapter-modal" onclick="chapterActionManager(${chapter.id}, 'update', '${detailUrl}')">
                    Edit
                </button>
            </td>
            <td>
                <button id="chapter-${chapter.id}" type="button" class="btn btn-danger" data-bs-toggle="modal" 
                data-bs-target="#chapter-modal" onclick="chapterActionManager(${chapter.id}, 'delete', '${detailUrl}')">
                    Delete
                </button>
            </td>
        </tr>
    `;
}

const addValuesInTable = (results, bodyId, url, name) => {
    const tBody = document.getElementById(bodyId);
    let htmlTBody = '';
    for (let index = 0; index < results.length; index++) {
        let result = results[index];
        let detailUrl = url + `${result.id}/`;
        if (name === "series")
            htmlTBody += createSeriesRow(index, detailUrl, result);
        else if (name === "chapters")
            htmlTBody += createChapterRow(index, detailUrl, result);
    }
    tBody.innerHTML = htmlTBody;
}

const prepareTable = (data, params) => {
    addValuesInHeader(params.columns_to_show, params.headerId);
    addValuesInTable(data, params.bodyId, params.url, params.name);
}

function getSeriesList() {
    let params = {
        columns_to_show: ['id', 'name', 'episode_list_url', 'actions', ''],
        headerId: "seriesHead",
        bodyId: "seriesBody",
        url: seriesURL,
        name: "series"
    }
    fetchData(seriesURL)
        .then(data => prepareTable(data.results, params))
        .catch(error => console.log(error))
}

function getRecentChapters() {
    let params = {
        columns_to_show: ['id', 'series name', 'title', 'release_date', 'is_seen', 'actions', ''],
        headerId: "recentChapterHead",
        bodyId: "recentChapterBody",
        url: chapterURL,
        name: "chapters",
    }
    fetchData(chapterURL + "recent/")
        .then(data => prepareTable(data, params))
        .catch(error => console.log(error))
}

function getSeenChapters() {
    let params = {
        columns_to_show: ['id', 'series name', 'title', 'release_date', 'is_seen', 'actions', ''],
        headerId: "seenChapterHead",
        bodyId: "seenChapterBody",
        url: chapterURL,
        name: "chapters",
    }
    fetchData(chapterURL + "seen/")
        .then(data => prepareTable(data, params))
        .catch(error => console.log(error))
}

function getUnSeenChapters() {
    let params = {
        columns_to_show: ['id', 'series name', 'title', 'release_date', 'is_seen', 'actions', ''],
        headerId: "UnSeenChapterHead",
        bodyId: "UnSeenChapterBody",
        url: chapterURL,
        name: "chapters",
    }
    fetchData(chapterURL + "notseen/")
        .then(data => prepareTable(data, params))
        .catch(error => console.log(error))
}

getSeriesList();
getRecentChapters();
getSeenChapters();
getUnSeenChapters();

// End Tables
// --------------------------------------------------------------------------------


// --------------------------------------------------------------------------------
// Forms

const parseFormDataToJSON = (form) => {
    const formData = new FormData(form);
    const plainFormData = Object.fromEntries(formData.entries());
    return JSON.stringify(plainFormData);
}

function handleSeriesFormSubmit(event) {
    const form = event.currentTarget;
    const formJsonData = parseFormDataToJSON(form);
    postSeriesForm(formJsonData)
        .then(response => console.log(response))
        .catch(error => console.error(error));
}

function handlerActionForm(event) {
    const form = event.currentTarget;
    const formJsonData = parseFormDataToJSON(form);
    const method = form.getAttribute('data-method');
    const endpointUrl = form.getAttribute('endpoint');
    fetchActionForm(formJsonData, method, endpointUrl)
        .then(response => console.log(response))
        .catch(error => console.error(error));
}

const seriesForm = document.getElementById('series-form');
seriesForm.addEventListener("submit", (event) => handleSeriesFormSubmit(event));

const seriesActionForm = document.getElementById('series-action-form');
seriesActionForm.addEventListener('submit', handlerActionForm);

const chapterActionForm = document.getElementById('chapter-action-form');
chapterActionForm.addEventListener('submit', handlerActionForm);


// End Forms
// -------------------------------------------------


// -------------------------------------------------
// Modals

const seriesActionManager = async (seriesId, action, baseUrl) => {
    const serieInfo = await fetchData(baseUrl);
    const modal = document.getElementById("series-modal");

    const modalTitle = modal.querySelector('.modal-title');
    const actionMessage = modal.querySelector('.modal-body p');
    const seriesNameInput = modal.querySelector('.modal-body input[id="series-name"]');
    const seriesUrlInput = modal.querySelector('.modal-body textarea[id="series-url"]');

    modalTitle.textContent = action.charAt(0).toUpperCase() + action.slice(1) + " a series";
    actionMessage.textContent = "Are you sure you want to " + action + " this series?";
    seriesNameInput.value = serieInfo.name;
    seriesUrlInput.value = serieInfo.episode_list_url;

    const method = action === "update" ? "PUT" : "DELETE";
    const formAction = document.getElementById('series-action-form');
    formAction.setAttribute("data-method", method);
    formAction.setAttribute("id_series", seriesId);
    formAction.setAttribute("endpoint", baseUrl);
}

const chapterActionManager = async (chapterId, action, baseUrl) => {
    const chapterInfo = await fetchData(baseUrl);
    const modal = document.getElementById("chapter-modal");

    const modalTitle = modal.querySelector('.modal-title');
    const actionMessage = modal.querySelector('.modal-body p');
    const chapterSeriesIdInput = modal.querySelector('.modal-body input[id="series-id-name"]');
    const chapterTitleInput = modal.querySelector('.modal-body input[id="chapter-title"]');
    const chapterLinkInput = modal.querySelector('.modal-body input[id="chapter-link"]');
    const chapterReleaseDate = modal.querySelector('.modal-body input[id="chapter-release-date"]');
    const chapterSeen = modal.querySelector('.modal-body input[type="checkbox"]');

    modalTitle.textContent = action.charAt(0).toUpperCase() + action.slice(1) + " a chapter";
    actionMessage.textContent = "Are you sure you want to " + action + " this chapter?";
    chapterSeriesIdInput.value = chapterInfo.serie_name;
    chapterTitleInput.value = chapterInfo.title;
    chapterLinkInput.value = chapterInfo.link;
    chapterReleaseDate.value = chapterInfo.release_date;
    chapterSeen.checked = chapterInfo.is_seen;

    const method = action === "update" ? "PUT" : "DELETE";
    const formAction = document.getElementById('chapter-action-form');
    formAction.setAttribute("data-method", method);
    formAction.setAttribute("id_chapter", chapterId);
    formAction.setAttribute("endpoint", baseUrl);
}



// End Modals
// --------------------------------------------------------

// Para evitar el "confirmar reenvio de formulario".
if (window.history.replaceState){
    window.history.replaceState(null, null, window.location.href);
}

from django.db import IntegrityError
from django.test import TestCase
from django.urls import reverse

from series.models import Serie, Chapter
from series.scrapers import HTMLScraper
from series.tasks import select_scraper, scrap_last_chapter, save_chapter

from rest_framework.test import APITestCase


# Create your tests here.

class TestSeries(TestCase):
    """Testing Serie model"""

    def test_series_name_required(self):
        """Testing series.Serie model has a 'name' column as not null."""
        serie = Serie(
            episode_list_url="https://lectortmo.com/library/manga/10457/black-clover"
        )
        with self.assertRaisesMessage(IntegrityError, expected_message="NOT NULL constraint failed: series_serie.name"):
            serie.save()

    def test_series_episode_list_url_required(self):
        """Testing series.Serie model has a 'episode_list_url' column as not null."""
        serie = Serie(
            name="Black clover"
        )
        with self.assertRaises(IntegrityError):
            serie.save()

    def test_series_episode_list_url_valid(self):
        """Testing 'episode_list_url' has a valid url."""
        serie = Serie(
            name="Solo leveling",
            episode_list_url=""
        )
        with self.assertRaisesMessage(IntegrityError, expected_message="Enter a valid URL"):
            serie.save()

    def test_series_valid_values(self):
        """Testing 'name' and 'episode_list_url' have valid values."""
        serie = Serie(
            name="Tower of god",
            episode_list_url="https://www.webtoons.com/en/fantasy/tower-of-god/list?title_no=95"
        )
        serie.save()

    def test_series_episode_list_url_is_supported(self):
        """
        Testing 'episode_list_url' value is supported by the app.
        The app only supports Lectortmo and Webtoon websites.
        Each "kind" of supported website can be scraped in a specific way.
        There are two (in total) available scrapers for both lectortmo and webtoon websites.
        """
        urls = [
            'https://www.webtoons.com/es/super-hero/unordinary/list?title_no=1914',
            'https://lectortmo.com/library/manhwa/120/solo-leveling',
            'https://www.webtoons.com/en/action/omniscient-reader/list?title_no=2154'
        ]
        for url in urls:
            scraper = select_scraper(url)
            self.assertIsInstance(scraper, HTMLScraper)

    def test_series_episode_list_url_not_supported(self):
        """
        Testing 'episode_list_url' value is not supported by the app.
        The app only supports Lectortmo and Webtoon websites.
        Each "kind" of supported website can be scraped in a specific way.
        There are two (in total) available scrapers for both lectortmo and webtoon websites.
        """
        urls = [
            'https://e-learning-car.lsv-tech.com/my/',
            'https://stackoverflow.com/',
            'https://github.com/',
            'https://mangadex.org/title/e7eabe96-aa17-476f-b431-2497d5e9d060/black-clover',
            'https://mangareader.cc/manga/bleach',
        ]
        for url in urls:
            scraper = select_scraper(url)
            self.assertNotIsInstance(scraper, HTMLScraper)


class TestTasks(TestCase):
    """Testing celery tasks"""

    def test_task_last_chapter_scraper(self):
        """
        Given a supported url of an episode list from a series, get the last chapter.
        The scraping process is done by a celery task.
        """
        url = 'https://lectortmo.com/library/manga/150/naruto'
        last_chapter = scrap_last_chapter(url)
        self.assertIsInstance(last_chapter, dict)
        self.assertTrue(last_chapter)

    def test_task_save_chapter(self):
        """
        Testing saving a scraped chapter info in Chapter table.
        The saving process is done through a celery task.
        """
        serie = Serie(
            name="Omniscient Reader",
            episode_list_url='https://www.webtoons.com/en/action/omniscient-reader/list?title_no=2154'
        )
        serie.save()
        chapter = scrap_last_chapter(serie.episode_list_url)
        save_chapter(chapter)
        num_chapters = Chapter.objects.count()
        self.assertEquals(num_chapters, 1)

    def test_task_save_series_required(self):
        """Testing saving a series is required to save a chapter."""
        chapter = {
            'title': ' Capítulo 702.00:  El Chico del Sharingan',
            'link': 'https://lectortmo.com/view_uploads/80490',
            'release_date': '2015-05-08',
            'episode_list_url': 'https://lectortmo.com/library/manga/150/naruto'
        }
        with self.assertRaisesMessage(Serie.DoesNotExist, expected_message="Serie matching query does not exist."):
            save_chapter(chapter)


class TestRestAPI(APITestCase):
    """Testing Rest API"""

    def test_api_series_list(self):
        url = '/api/series/'
        response = self.client.get(path=url)
        self.assertEquals(response.status_code, 200)

    def test_api_series_post(self):
        url = '/api/series/'
        data = {
            'name': "Tower of god",
            'episode_list_url': "https://www.webtoons.com/en/fantasy/tower-of-god/list?title_no=95"
        }
        response = self.client.post(url, data, format='json')
        self.assertEquals(response.status_code, 201)

    def test_api_series_retrieve(self):
        get_url = '/api/series/'
        data = {
            'name': "Tower of god",
            'episode_list_url': "https://www.webtoons.com/en/fantasy/tower-of-god/list?title_no=95"
        }
        self.client.post(get_url, data, format='json')
        series_id = Serie.objects.get(name='Tower of god')
        detail_url = f'/api/series/{series_id.id}/'
        get_response = self.client.get(detail_url)
        self.assertEquals(get_response.status_code, 200)

    def test_api_series_put(self):
        get_url = '/api/series/'
        data = {
            'name': "Tower of god",
            'episode_list_url': "https://www.webtoons.com/en/fantasy/tower-of-god/list?title_no=95"
        }
        self.client.post(get_url, data, format='json')
        series = Serie.objects.get(name='Tower of god')
        detail_url = f'/api/series/{series.id}/'

        new_data = {
            'name': "Tower of god hiiii",
            'episode_list_url': "https://www.webtoons.com/en/fantasy/tower-of-god/list?title_no=95"
        }
        put_response = self.client.put(path=detail_url, data=new_data, format='json')
        self.assertEquals(put_response.status_code, 200)

        mod_series = Serie.objects.get(name='Tower of god hiiii')
        self.assertNotEquals(mod_series, None)

    def test_api_series_delete(self):
        url = '/api/series/'
        data = {
            'name': "Tower of god",
            'episode_list_url': "https://www.webtoons.com/en/fantasy/tower-of-god/list?title_no=95"
        }
        self.client.post(url, data, format='json')
        series_id = Serie.objects.get(name='Tower of god')
        delete_url = f'/api/series/{series_id.id}/'
        get_response = self.client.delete(delete_url)
        self.assertEquals(get_response.status_code, 204)

        with self.assertRaisesMessage(Serie.DoesNotExist, expected_message="Serie matching query does not exist."):
            Serie.objects.get(name='Tower of god')

    def test_api_chapter_list(self):
        url = '/api/chapters/'
        response = self.client.get(path=url)
        self.assertEquals(response.status_code, 200)

    def test_api_chapter_retrieve(self):
        serie = Serie.objects.create(
            name='Oni-sama 2.0',
            episode_list_url='https://www.webtoons.com/es/action/teenage-mercenary/list?title_no=3252'
        )
        chapter = Chapter.objects.create(
            title="Ep. 60",
            link="https://www.webtoons.com/es/action/teenage-mercenary/ep-60/viewer?title_no=3252&episode_no=61",
            release_date="02-may-2022",
            serie_id=serie,
        )
        chapter_id = Chapter.objects.get(title=chapter.title)
        retrieve_url = f'/api/chapters/{chapter_id.id}/'
        response = self.client.get(retrieve_url)
        self.assertEquals(response.status_code, 200)

    def test_api_chapter_create_valid_data(self):
        """
        Testing create method by passing valid data to serializer.
        Status code response expected is 200.
        """
        serie = Serie.objects.create(
            name='Oni-sama 2.0',
            episode_list_url='https://www.webtoons.com/es/action/teenage-mercenary/list?title_no=3252'
        )
        post_url = '/api/chapters/'
        chapter = {
            "serie_id": serie.id,
            "title": "Ep. 60",
            "link": "https://www.webtoons.com/es/action/teenage-mercenary/ep-60/viewer?title_no=3252&episode_no=61",
            "release_date": "02-may-2022",
        }
        response = self.client.post(path=post_url, data=chapter)
        self.assertEquals(response.status_code, 201)

    def test_api_chapter_create_not_valid_data(self):
        """
        Testing create method by passing not valid data to serializer.
        Status code response expected is 400 bad request.
        """
        serie = Serie.objects.create(
            name='Oni-sama 2.0',
            episode_list_url='https://www.webtoons.com/es/action/teenage-mercenary/list?title_no=3252'
        )
        post_url = '/api/chapters/'
        chapter = {
            "serie_id": serie,  # Not a valid serie_id value
            "title": "Ep. 60",
            "link": "https://www.webtoons.com/es/action/teenage-mercenary/ep-60/viewer?title_no=3252&episode_no=61",
            "release_date": "02-may-2022",
        }
        response = self.client.post(path=post_url, data=chapter)
        self.assertEquals(response.status_code, 400)

    def test_api_chapter_update_valid_data(self):
        """
        Testing update method by passing valid data to serializer.
        Status code response expected is 200.
        """
        serie = Serie.objects.create(
            name='Oni-sama 2.0',
            episode_list_url='https://www.webtoons.com/es/action/teenage-mercenary/list?title_no=3252'
        )
        chapter = Chapter.objects.create(
            title="Ep. 60",
            link="https://www.webtoons.com/es/action/teenage-mercenary/ep-60/viewer?title_no=3252&episode_no=61",
            release_date="02-may-2022",
            serie_id=serie,
        )
        chapter_id = Chapter.objects.get(title=chapter.title)
        put_url = f'/api/chapters/{chapter_id.id}/'
        new_chapter = {
            "serie_id": serie.id,
            "title": "Ep. 60",
            "link": "https://www.webtoons.com/es/action/teenage-mercenary/ep-60/viewer?title_no=3252&episode_no=61",
            "release_date": "02-may-2022",
            "is_seen": "on"
        }
        response = self.client.put(path=put_url, data=new_chapter)
        self.assertEquals(response.status_code, 200)

    def test_api_chapter_update_not_valid_data(self):
        """
        Testing update method by passing not valid data to serializer.
        Status code response expected is 400 bad request.
        """
        serie = Serie.objects.create(
            name='Oni-sama 2.0',
            episode_list_url='https://www.webtoons.com/es/action/teenage-mercenary/list?title_no=3252'
        )
        chapter = Chapter.objects.create(
            title="Ep. 60",
            link="https://www.webtoons.com/es/action/teenage-mercenary/ep-60/viewer?title_no=3252&episode_no=61",
            release_date="02-may-2022",
            serie_id=serie,
        )
        chapter_id = Chapter.objects.get(title=chapter.title)
        put_url = f'/api/chapters/{chapter_id.id}/'
        new_chapter = {
            "serie_id": serie,  # Not a valid serie_id value
            "title": "Ep. 60",
            "link": "https://www.webtoons.com/es/action/teenage-mercenary/ep-60/viewer?title_no=3252&episode_no=61",
            "release_date": "02-may-2022",
            "is_seen": "on"
        }
        response = self.client.put(path=put_url, data=new_chapter)
        self.assertEquals(response.status_code, 400)

    def test_api_chapter_delete(self):
        serie = Serie.objects.create(
            name='Oni-sama 2.0',
            episode_list_url='https://www.webtoons.com/es/action/teenage-mercenary/list?title_no=3252'
        )
        chapter = Chapter.objects.create(
            title="Ep. 60",
            link="https://www.webtoons.com/es/action/teenage-mercenary/ep-60/viewer?title_no=3252&episode_no=61",
            release_date="02-may-2022",
            serie_id=serie,
        )

        db_chapter = Chapter.objects.get(title=chapter.title)
        del_url = f'/api/chapters/{db_chapter.id}/'
        response = self.client.delete(del_url)
        self.assertEquals(response.status_code, 204)

    def test_api_chapter_recent(self):
        url = '/api/chapters/recent/'
        response = self.client.get(path=url)
        self.assertEquals(response.status_code, 200)

    def test_api_chapter_is_seen(self):
        url = '/api/chapters/seen/'
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_api_chapter_not_seen(self):
        url = '/api/chapters/notseen/'
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)


class TestOthers(TestCase):
    def test_frontend_home(self):
        url = reverse('home')
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "Series Scraper-Tracker")

from rest_framework import serializers
from series.models import Serie, Chapter


class SerieSerializer(serializers.ModelSerializer):
    """Serializer for Serie model."""

    class Meta:
        model = Serie
        fields = ['id', 'name', 'episode_list_url']


class ChapterSerializer(serializers.ModelSerializer):
    """Serializer for Chapter model."""
    serie_name = serializers.SerializerMethodField('get_serie_name')

    # serie_id = serializers.StringRelatedField()
    # serie_id = SerieSerializer().fields['name']

    class Meta:
        model = Chapter
        fields = ['id', 'title', 'link', 'release_date', 'is_seen', 'serie_id', 'serie_name']

    def get_serie_name(self, chapter):
        serie_name = chapter.serie_id.name
        return serie_name

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.link = validated_data.get('link', instance.link)
        instance.release_date = validated_data.get('release_date', instance.release_date)
        instance.is_seen = validated_data.get('is_seen', instance.is_seen)

        Chapter.objects.filter(id=instance.id).update(
            title=instance.title,
            link=instance.link,
            release_date=instance.release_date,
            is_seen=instance.is_seen,
        )
        return instance

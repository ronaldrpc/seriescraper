import dateutil.parser
import calendar
import requests
import time

BLACK_CLOVER_URL = "https://lectortmo.com/library/manga/10457/black-clover"
SOLO_LEVELING_URL = "https://lectortmo.com/library/manhwa/120/solo-leveling"
EXTRAORDINARIO_URL = "https://www.webtoons.com/es/super-hero/unordinary/list?title_no=1914"
OMNISCIENT_READER_URL = "https://www.webtoons.com/en/action/omniscient-reader/list?title_no=2154"
TOWER_OF_GOD_URL = "https://www.webtoons.com/en/fantasy/tower-of-god/list?title_no=95"

HEADERS = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36',
}


def request_url(url):
    """Returns the response of a request to an url through GET method."""
    return requests.get(url, headers=HEADERS)


# def convert_date_to_epoch_timestamp(str_date):
#     """Returns the epoch (Unix timestamp) that represents a given date."""
#     parsed_date = dateutil.parser.parse(str_date)
#     str_parsed_date = parsed_date.strftime("%Y-%m-%d")
#     epoch = calendar.timegm(time.strptime(str_parsed_date, '%Y-%m-%d'))
#
#     # Offset in seconds
#     GMT_5 = 18000
#     return epoch + GMT_5

from django.contrib import admin
from series.models import Serie, Chapter
from seriescraper.utils import BaseModelAdmin


# Register your models here.
@admin.register(Serie)
class SerieAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'episode_list_url', 'created_at', 'updated_at']
    list_filter = ['name']
    search_fields = ['name']


@admin.register(Chapter)
class ChapterAdmin(BaseModelAdmin):
    list_display = ['id', 'title', 'link', 'release_date', 'is_seen', 'serie_id', 'created_at', 'updated_at']
    list_filter = ['is_seen', 'title', 'updated_at']
    search_fields = ['title']

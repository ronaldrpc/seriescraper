from series.utils import request_url
from series.models import Serie, Chapter
from series.factory import LectortmoFactory, WebtoonFactory
from celery import shared_task


def select_scraper(url):
    """Choose a scraper based on the website name in the given url."""
    available_scrapers = {
        'lectortmo.com': LectortmoFactory().get_scraper(),
        'webtoons.com': WebtoonFactory().get_scraper(),
    }
    for scraper_name in available_scrapers:
        if scraper_name in url:
            return available_scrapers[scraper_name]


def setup_scraper_request(url):
    """
    Configure the scraper selecting the correct one and making a request to an url.
    Returns the chosen scraper and the response of the request.
    """
    scraper = select_scraper(url)
    response = request_url(url)
    return scraper, response.text


def find_last_chapter(scraper, html):
    """
    Find the last chapter information given a chapters list page (in html format).
    Returns the chapter info as a dictionary."""
    scraper.set_episode_list(html)
    scraper.scrap_last_chapter()
    chapter = scraper.get_last_chapter_info()
    return chapter


def add_more_info(chapter, url):
    """Add the episode list url to a given chapter info."""
    chapter['episode_list_url'] = url
    # chapter['timestamp'] = convert_date_to_epoch_timestamp(chapter['release_date'])


@shared_task
def scrap_last_chapter(url):
    """
    Scraps the last chapter from an episode list page given an url.
    Returns the chapter information with the episode list added.
    """
    scraper, html = setup_scraper_request(url)
    chapter = find_last_chapter(scraper, html)
    add_more_info(chapter, url)
    return chapter


@shared_task
def process_series_data(link):
    """
    Call functions to scrap and save the last chapter scrapped for each series.
    These calls are linked by celery chain."""
    (scrap_last_chapter.s(link) | save_chapter.s())()


@shared_task
def run_last_chapter_series_workflow():
    """Call a function that process each series link and save the scraped data."""
    links = Serie.objects.all().values_list('episode_list_url', flat=True)
    for link in links:
        process_series_data.delay(link)
        # (scrap_last_chapter.s(link) | save_chapter.s())()


@shared_task
def save_chapter(chapter):
    """
    Save a given chapter info (dict) in the database.
    If a chapter already exists, the function updates it.
    """
    serie = Serie.objects.get(episode_list_url=chapter['episode_list_url'])
    Chapter.objects.update_or_create(
        title=chapter['title'],
        link=chapter['link'],
        release_date=chapter['release_date'],
        defaults={
            'title': chapter['title'],
            'link': chapter['link'],
            'release_date': chapter['release_date'],
            'serie_id': serie
        }
    )

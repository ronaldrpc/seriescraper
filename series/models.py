from django.core.exceptions import ValidationError
from django.core.validators import URLValidator
from django.db import models, IntegrityError
from django.db.models import Max

from seriescraper.utils import BaseModel


# Create your models here.
class Serie(BaseModel):
    name = models.CharField(max_length=100, default=None)
    episode_list_url = models.URLField(max_length=255, default=None)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        try:
            validate = URLValidator()
            validate(self.episode_list_url)
        except ValidationError as error:
            raise IntegrityError(error.message)
        super().save(*args, **kwargs)


class ChapterManager(models.Manager):
    def last_chapters_by_series(self):
        series_with_last_chapter = Serie.objects.annotate(recent_chapter=Max('chapter__updated_at'))
        chapter_ids = series_with_last_chapter.values_list('recent_chapter', flat=True)
        return self.filter(updated_at__in=chapter_ids)

    def is_seen(self):
        return self.filter(is_seen=True)

    def not_seen(self):
        return self.filter(is_seen=False)


class Chapter(BaseModel):
    title = models.CharField(max_length=100)
    link = models.URLField(max_length=255)
    release_date = models.CharField(max_length=50)
    is_seen = models.BooleanField(default=False)
    serie_id = models.ForeignKey(Serie, on_delete=models.CASCADE)

    objects = ChapterManager()



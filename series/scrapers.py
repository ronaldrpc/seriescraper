import re
import unicodedata

from bs4 import BeautifulSoup


class HTMLScraper:
    def __init__(self):
        self.scraper = None
        self.last_chapter_info = None

    def set_episode_list(self, html):
        self.scraper = BeautifulSoup(html, 'html.parser')

    def get_scraper(self):
        return self.scraper

    def get_last_chapter_info(self):
        return self.last_chapter_info

    def find_last_chapter(self):
        pass

    def scrap_last_chapter(self):
        pass

    def scrap_chapter_info(self, chapter):
        pass


class LectortmoScraper(HTMLScraper):
    """Customized scraper for lectortmo.com website"""

    def __init__(self):
        super().__init__()

    def scrap_last_chapter(self):
        """Find the html element that contains information about the last chapter from a list of episodes. """
        chapter_lists = self.scraper.find('div', attrs={"id": "chapters"})
        last_chapter = chapter_lists.find("li", attrs={"data-index": "0"})
        self.last_chapter_info = self.scrap_chapter_info(last_chapter)

    def scrap_chapter_info(self, chapter):
        """Retrieve the title, link and date release of the last chapter in lectortmo website."""
        regex_link = re.compile(r"^https://lectortmo\.com/view_uploads/")
        title = str(chapter.find("a", attrs={"class": "btn-collapse"}).contents[1])
        link = chapter.find("a", attrs={"href": regex_link}).get('href')
        release_date = str(chapter.find("i", attrs={"class": "fa-calendar"}).parent.contents[1]).strip()

        # Convert &nbsp (non-braking space) character to unicode string
        title_normalized = unicodedata.normalize('NFKC', title)
        chapter_info = {
            'title': title_normalized,
            'link': link,
            'release_date': release_date,
        }
        return chapter_info


class WebtoonScraper(HTMLScraper):
    """Customized scraper for webtoons.com website"""

    def __init__(self):
        super().__init__()

    def scrap_last_chapter(self):
        chapter_lists = self.scraper.find('ul', attrs={"id": "_listUl"})
        last_chapter = chapter_lists.contents[1]
        self.last_chapter_info = self.scrap_chapter_info(last_chapter)

    def scrap_chapter_info(self, chapter):
        link = chapter.find("a").get('href')
        title = str(chapter.find("span", attrs={"class": "subj"}).contents[0].string)
        release_date = str(chapter.find("span", attrs={"class": "date"}).string)
        chapter_info = {
            'title': title,
            'link': link,
            'release_date': release_date,
        }
        return chapter_info

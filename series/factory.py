from series.scrapers import HTMLScraper, LectortmoScraper, WebtoonScraper
from abc import ABC


class ScraperFactory(ABC):
    """Factory that provides a scraper instance."""

    def get_scraper(self) -> HTMLScraper:
        """Returns a new scraper instance."""


class LectortmoFactory(ScraperFactory):
    """Factory that provides a lectortmo scraper instance."""

    def get_scraper(self) -> HTMLScraper:
        return LectortmoScraper()


class WebtoonFactory(ScraperFactory):
    """Factory that provides a Webtoon scraper instance."""

    def get_scraper(self) -> HTMLScraper:
        return WebtoonScraper()






from django.shortcuts import render, get_object_or_404
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from series.models import Serie, Chapter
from series.serializers import SerieSerializer, ChapterSerializer
from series.tasks import process_series_data


# Create your views here.

class SerieViewSet(viewsets.ModelViewSet):
    """
    Viewset (ModelViewSet) for Serie model.
    This viewset provides automatically CRUD methods.
    """
    queryset = Serie.objects.all()
    serializer_class = SerieSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        process_series_data(request.data.get('episode_list_url'))
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class ChapterViewSet(viewsets.ViewSet):
    """Viewset for Chapter model."""
    queryset = Chapter.objects.all()

    def list(self, request, queryset=queryset):
        serializer = ChapterSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = ChapterSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk):
        chapter = get_object_or_404(self.queryset, pk=pk)
        serializer = ChapterSerializer(chapter)
        return Response(serializer.data)

    def update(self, request, pk):
        chapter = get_object_or_404(self.queryset, pk=pk)
        serializer = ChapterSerializer(chapter, data=request.data)
        if serializer.is_valid():
            serializer.update(chapter, serializer.validated_data)
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk):
        chapter = get_object_or_404(self.queryset, pk=pk)
        chapter.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=False)
    def recent(self, request, *args, **kwargs):
        queryset = Chapter.objects.last_chapters_by_series()
        return self.list(request, queryset)

    @action(detail=False)
    def seen(self, request, *args, **kwargs):
        queryset = Chapter.objects.is_seen()
        return self.list(request, queryset)

    @action(detail=False)
    def notseen(self, request, *args, **kwargs):
        queryset = Chapter.objects.not_seen()
        return self.list(request, queryset)
